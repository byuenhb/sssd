FROM buildhost.test.suse.hk:5000/suse/sle15:15.3.17.5.33
arg repo
arg cert
arg hostname
arg hostfqdn
arg hostip

# Build the host table entry for suse manager server
RUN echo "$hostip $hostfqdn $hostname" > /root/hosts

# Import the crt file of RMT server
RUN echo "$cert" > /etc/pki/trust/anchors/RHN-ORG-TRUSTED-SSL-CERT.pem
RUN update-ca-certificates

# Copy repo
RUN echo "$repo" > /etc/zypp/repos.d/susemanager\:dockerbuild.repo

ADD add_packages.sh /root/add_packages.sh
#SHELL ["bin/bash","-c"]

# Add configuration files
ADD etc/samba/smb.conf /etc/samba/smb.conf
ADD etc/sssd/sssd.conf /etc/sssd/sssd.conf
ADD etc/krb5.conf /etc/krb5.conf
ADD etc/nscd.conf /etc/nscd.conf
ADD etc/pam.d/common-auth-pc /etc/pam.d/common-auth-pc
ADD etc/pam.d/common-account-pc /etc/pam.d/common-account-pc
ADD etc/pam.d/common-password-pc /etc/pam.d/common-password-pc
ADD etc/pam.d/common-session-pc /etc/pam.d/common-session-pc


# Remove container-suseconnect to avoid warning messages when running zypper
RUN rpm -e container-suseconnect

# Execute installation of required packages in one layer 
RUN chmod u+x /root/add_packages.sh && /bin/bash /root/add_packages.sh

CMD ["/bin/bash","-c","cat /etc/os-release"]
